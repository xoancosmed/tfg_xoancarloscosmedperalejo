﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    [SerializeField] private InputField newInput;
    [SerializeField] private InputField connectInput;
    [SerializeField] private GameObject messagePanel;
    [SerializeField] private Text messageText;

    void Start ()
    {
        messagePanel.SetActive(false); // Hide the message panel
        newInput.text = Network.player.ipAddress; // Get the IP so the user can easyly know it
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
    }

    // Method executed when the user press the "Close" button in the message panel
    public void ClickCloseMessageButton()
    {
        messagePanel.SetActive(false);
    }

    // Method executed when the user press the button which creates a new collaborative  environment
    public void ClickNewButton()
    {
        NetInfo.ip = Network.player.ipAddress;
        NetInfo.port = NetInfo.DEFAULT_PORT;
        NetInfo.state = NetInfo.State.HOST;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Method executed when the user press the button which connects you to an existing collaborative  environment
    public void ClickConnectButton()
    {
        string text = connectInput.text;
        if (text != "")
        {
            // Split the IP and Port from the input text

            string ip = "", port = "";
            string[] split = text.Split(':');

            if (split.Length < 2)
            {
                ip = text;
            }
            else if (split.Length == 2)
            {
                ip = split[0];
                port = split[1];
            }
            else
            {
                messageText.text = "Formato de la IP y puerto incorrecto";
                messagePanel.SetActive(true);
                return;
            }

            // Check the IP and Port

            if (ip != "")
            {
                if (!CheckIPv4(ip))
                {
                    messageText.text = "Formato de la IP incorrecto";
                    messagePanel.SetActive(true);
                    return;
                }
                else NetInfo.ip = ip;
            }

            if (port != "")
            {
                if (!CheckPort(port))
                {
                    messageText.text = "Formato del puerto incorrecto";
                    messagePanel.SetActive(true);
                    return;
                }
                else NetInfo.port = int.Parse(port);
            }
        }

        NetInfo.state = NetInfo.State.CLIENT;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private bool CheckIPv4(string ip)
    {
        string[] split = ip.Split('.');
        if (split.Length != 4) return false; // With this we make sure that the IP has 4 numebrs

        byte temp;
        foreach(string part in split)
        {
            if (!byte.TryParse(part, out temp)) return false; // With this we amke sure that each number can be converted to a byte
            if (temp == 0 || temp == 255) return false; // The .0 is reserved for the net, and the .255 is for broadcast
        }
        
        return true;
    }

    private bool CheckPort(string port)
    {
        ushort portNumber = 0;

        if (ushort.TryParse(port, out portNumber)) // First we check if the port is a number of 16 bits (without sign)
        {
            if (portNumber > 1024 && portNumber <= 49151) return true; // The port can be from 0 up to 65535, but the port 0 is reserved by the TCP/IP protocol, the range 1-1024 is reserved by the operating system and requires root access, and the range from 49152 to 65535 are reserved to ephemeral ports.
            else return false;
        }
        else return false;
    }

}
