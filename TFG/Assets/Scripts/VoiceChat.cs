﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceChat : MonoBehaviour {

    /* ********************* *
     * ***** VARIABLES ***** *
     * ********************* */

    private static TeamSpeakClient ts3_client;

    [SerializeField] private GameObject connectButton;
    [SerializeField] private GameObject disconnectButton;

    /* ************************* *
     * ***** UNITY METHODS ***** *
     * ************************* */

    void Start ()
    {
        // Getting the TeamSpeak's client		
        ts3_client = TeamSpeakClient.GetInstance();

        // Enabling logging
        TeamSpeakClient.logErrors = true;
    }
	
	void Update ()
    {
		if (TeamSpeakClient.started == true)
        {
            connectButton.SetActive(false);
            disconnectButton.SetActive(true);
        }
        else
        {
            connectButton.SetActive(true);
            disconnectButton.SetActive(false);
        }
	}

    /* ********************** *
     * ***** UI METHODS ***** *
     * ********************** */

    public void Connect()
    {
        ts3_client.StartClient(NetInfo.VoiceService.ip, (uint) NetInfo.VoiceService.port, NetInfo.VoiceService.secret, UserInfo.userName, ref NetInfo.VoiceService.defaultChannel, NetInfo.VoiceService.defaultChannelPassword);
    }

    public void Disconnect()
    {
        ts3_client.StopConnection("");
        ts3_client.StopClient();
    }

}
