﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ExitPanel : MonoBehaviour {

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void CancelExit()
    {
        Hide();
    }

    public void DisconnectAndExit()
    {
        if (NetInfo.state == NetInfo.State.CLIENT) NetworkManager.singleton.StopClient();
        else if (NetInfo.state == NetInfo.State.HOST) NetworkManager.singleton.StopHost();
        else if (NetInfo.state == NetInfo.State.SERVER) NetworkManager.singleton.StopServer();
    
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

}
