﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DragDropObject : NetworkBehaviour
{

    public bool locked = false;
    [SyncVar] public string objectName;

    private EditObject editObject;

    private RaycastHit hit;

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool dragging = false;

    [SerializeField] float deepSpeed = 1.5f;
    [SerializeField] float rotateSpeed = 60f;

    [SerializeField] private GameObject nameLabelPrefab;
    private GameObject nameLabel = null;

    void Update()
    {
        if (editObject == null)
        {
            try
            {
                editObject = GameObject.Find("EditObject").GetComponent<EditObject>();
            }
            catch
            {
                return;
            }
        }

        // CHECK NAME

        if (objectName != null)
        {
            if (objectName.Trim() != "")
            {
                if (nameLabel == null) nameLabel = Instantiate(nameLabelPrefab, gameObject.transform);
                nameLabel.GetComponent<TextMesh>().text = objectName;
                //nameLabel.transform.LookAt(Camera.main.transform);
                nameLabel.transform.rotation = Quaternion.LookRotation(nameLabel.transform.position - Camera.main.transform.position);
            }
            else
            {
                if (nameLabel != null) Destroy(nameLabel);
            }
        }

        // SELECT THE OBJECT

        if (Input.GetMouseButtonDown(1))
        {
            if (!hasAuthority)
            {
                Debug.Log("!hasAuthority");
                if (locked) return;
                else
                {
                    Debug.Log("Requesting authority ...");
                    GameObject player = GameObject.FindGameObjectWithTag("Player");
                    NetworkIdentity playerId = player.GetComponent<NetworkIdentity>();
                    player.GetComponent<PlayerController>().CmdChangeAuthority(netId, playerId);
                    Debug.Log("Requested authority ...");
                }
            }

            try
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        if (!editObject.IsSelected(gameObject))
                        {
                            SelectObject();
                        }
                    }
                }
            }
            catch
            {
                Debug.LogWarning("No camera found!");
            }
        }

        // MOVE

        if (Input.GetMouseButtonDown(0))
        {
            if (!hasAuthority)
            {
                Debug.Log("!hasAuthority");
                if (locked) return;
                else
                {
                    Debug.Log("Requesting authority ...");
                    GameObject player = GameObject.FindGameObjectWithTag("Player");
                    NetworkIdentity playerId = player.GetComponent<NetworkIdentity>();
                    player.GetComponent<PlayerController>().CmdChangeAuthority(netId, playerId);
                    Debug.Log("Requested authority ...");
                }
            }

            try
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        if (editObject.IsSelected(gameObject) && editObject.IsMoving())
                        {
                            MouseDown();
                            dragging = true;
                        }
                    }
                }
            }
            catch
            {
                Debug.LogWarning("No camera found!");
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
        }

        if (dragging)
        {
            MouseDrag();
        }

        // ROTATE

        if (Input.GetMouseButton(1))
        {
            if (editObject.IsSelected(gameObject) && editObject.IsMoving())
            {
                transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * rotateSpeed, Space.World);
                /*float x = Input.GetAxis("Mouse X") * Time.deltaTime * rotateSpeed;
                float y = Input.GetAxis("Mouse Y") * Time.deltaTime * rotateSpeed;
                transform.Rotate(Vector3.up, -x);
                transform.Rotate(Vector3.right, y);*/
            }
        }
    }

    private void MouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    private void MouseDrag()
    {
        if (/*Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow) ||*/ Input.GetKey(KeyCode.Comma)) screenPoint.z += Time.deltaTime * deepSpeed;
        if (/*Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow) ||*/ Input.GetKey(KeyCode.Period)) screenPoint.z -= Time.deltaTime * deepSpeed;

        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    // DRAG AND DROP WITH LEFT BUTTON

    /*private Vector3 screenPoint;
    private Vector3 offset;

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }*/

    private void SelectObject()
    {
        editObject.Show(gameObject);
    }

}
