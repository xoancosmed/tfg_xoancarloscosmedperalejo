﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

    // %%%%%%%%%%%%%%%%% //
    // PRIVATE VARIABLES //
    // %%%%%%%%%%%%%%%%% //

    // Mouse sensitivity
    private float sensitivity = 5f;
    private float smoothness = 2f;

    // Zoom sensitivity
    private float zoomSensitivity = 10f;
    private float zoomMin = 15f;
    private float zoomMax = 90f;

    // Main camera and body of the current player
    private GameObject playerCamera;
    private GameObject playerBody;

    // Chat script (to check if the user is writing or scrolling)
    private Chat chat;

    // EditObject script (to check if the user is writing)
    private EditObject editObject;

    // Confirm exit panel
    private GameObject exitPanel;

    // Relative position of the mouse
    private Vector2 mouseLook;
    private Vector2 smoothV;

    // Movement
    [SerializeField] private float speed = 6f;

    // Prefabs
    [SerializeField] private GameObject interactibleCube;
    [SerializeField] private GameObject interactibleSphere;
    [SerializeField] private GameObject interactibleCake;
    [SerializeField] private GameObject interactibleCauldron;
    [SerializeField] private GameObject interactibleTree;

    // My spawned objects
    private List<GameObject> spawnedObjects;

    // %%%%%%%%%%%%% //
    // UNITY METHODS //
    // %%%%%%%%%%%%% //

    void Start () {

        // Get the camera of this player
        playerCamera = gameObject.transform.Find("Camera").gameObject;

        // Get the body of this player
        playerBody = gameObject.transform.Find("Body").gameObject;

        // Get the chat (to check if the user is writing or scrolling)
        chat = GameObject.Find("Chat").GetComponent<Chat>();

        // Get the EditObject (to check if the user is writing)
        editObject = GameObject.Find("EditObject").GetComponent<EditObject>();

        // Confirm exit panel
        exitPanel = GameObject.Find("ExitConfirmationPanel");
        if (exitPanel == null) Debug.LogWarning("Unable to get ExitConfirmationPanel");
        else
        {
            exitPanel.GetComponent<ExitPanel>().Hide();
        }

        // Disable the camera if is not yours
        if (!isLocalPlayer) playerCamera.SetActive(false);
        else playerCamera.tag = "MainCamera";

        // Hide your own body
        if (isLocalPlayer) playerBody.GetComponent<Renderer>().enabled = false;

        // Initialize the variables
        spawnedObjects = new List<GameObject>();

    }
	
	void Update () {

        // If this is not the local player, we skip the movement code
        if (!isLocalPlayer) return;

        // ESC

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (exitPanel.activeSelf) exitPanel.GetComponent<ExitPanel>().Hide();
            else if (editObject.IsMoving()) editObject.DisableMovement();
            else if (editObject.IsEditing()) editObject.Close();
            else exitPanel.GetComponent<ExitPanel>().Show();
        }

        // MOVEMENT (WITH THE KEYBOARD)

        if (!(chat.IsWriting() || editObject.IsWriting()))
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                transform.Translate(Vector3.forward * speed * Time.deltaTime * 1.5f);

            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                transform.Translate(-Vector3.forward * speed * Time.deltaTime * 0.5f);

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                transform.Translate(-Vector3.right * speed * Time.deltaTime);

            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                transform.Translate(Vector3.right * speed * Time.deltaTime);
        }

        // ZOOM WITH THE MOUSE

        float zoom = Camera.main.fieldOfView;
        zoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
        zoom = Mathf.Clamp(zoom, zoomMin, zoomMax);
        Camera.main.fieldOfView = zoom;

        // ROTATION WITH THE MOUSE (DRAG AND DROP WITH LEFT KEY)

        if (!chat.IsScrolling())
        {
            // If we are moving an object, we have to press Ctrl. If we are not moving an objet, we don't have to.
            if ((!editObject.IsMoving()) || ModifierKey())
            {
                if (Input.GetMouseButton(0))
                {
                    Vector2 md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

                    md = Vector2.Scale(md, new Vector2(sensitivity * smoothness, sensitivity * smoothness));
                    smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothness);
                    smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothness);

                    mouseLook += smoothV;
                    mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);

                    playerCamera.transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                    transform.localRotation = Quaternion.AngleAxis(mouseLook.x, transform.up);
                }
            }
        }

        // If we are moving, rotating or scaling an object, we skip
        if (editObject.IsMoving())
        {
            if (Input.GetKeyDown(KeyCode.Z) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand)))
            {
                editObject.UndoMovement();
            }

            return;
        }

        // INSTANCIATE AN OBJECT

        if (ModifierKey())
        {
            int number = NumberKey();
            if (number >= 0 && number < 10) CmdNetworkInstanciate(number);
        }

        // DELETE OBJECT(S)

        if (Input.GetKeyDown(KeyCode.X))
        {
            if (ModifierKey()) CmdDeleteAllObjects();
            else CmdDeleteLastObject();
        }

    }

    [Command] private void CmdDeleteAllObjects()
    {
        if (spawnedObjects.Count > 0)
        {
            foreach (GameObject spawnedObject in spawnedObjects)
            {
                if (spawnedObject != null) NetworkServer.Destroy(spawnedObject);
            }
        }
    }

    [Command] private void CmdDeleteLastObject()
    {
        if (spawnedObjects.Count > 0)
        {
            if (spawnedObjects[spawnedObjects.Count - 1] != null)
            {
                NetworkServer.Destroy(spawnedObjects[spawnedObjects.Count - 1]);
                spawnedObjects.RemoveAt(spawnedObjects.Count - 1);
            }
        }
    }

    [Command] public void CmdNetworkInstanciate(int number)
    {
        GameObject prefab = null;

        switch (number)
        {
            case 0:
                prefab = interactibleCube;
                break;

            case 1:
                prefab = interactibleSphere;
                break;

            case 2:
                prefab = interactibleCake;
                break;

            case 3:
                prefab = interactibleCauldron;
                break;

            case 4:
                prefab = interactibleTree;
                break;

            case 5:
                break;

            case 6:
                break;

            case 7:
                break;

            case 8:
                break;

            case 9:
                break;

            default:
                break;
        }

        if (prefab != null)
        {
            GameObject instanciatedObject = Instantiate(prefab, transform.position + Vector3.forward * 2 + Vector3.up * 0.5f, transform.rotation);
            //NetworkServer.Spawn(instanciatedObject); // Remind that all the spawmable objets must be registered in the network manager
            NetworkServer.SpawnWithClientAuthority(instanciatedObject, connectionToClient);

            spawnedObjects.Add(instanciatedObject);
        }
    }

    private bool ModifierKey()
    {
        bool shift = Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift);
        bool alt = Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.AltGr);
        bool ctrl = Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl);
        bool cmd = Input.GetKey(KeyCode.RightCommand) || Input.GetKey(KeyCode.LeftCommand);

        return (shift || alt || ctrl || cmd);
    }

    private int NumberKey()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0)) return 0;
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) return 1;
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) return 2;
        if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) return 3;
        if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4)) return 4;
        if (Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5)) return 5;
        if (Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Keypad6)) return 6;
        if (Input.GetKeyDown(KeyCode.Alpha7) || Input.GetKeyDown(KeyCode.Keypad7)) return 7;
        if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Keypad8)) return 8;
        if (Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9)) return 9;

        return -1;
    }

    [Command] public void CmdChangeAuthority (NetworkInstanceId objectNetId, NetworkIdentity player)
    {
        Debug.Log("[Inicio] CmdChangeAuthority");
        GameObject netObject = NetworkServer.FindLocalObject(objectNetId);
        NetworkIdentity netIdentity = netObject.GetComponent<NetworkIdentity>();
        NetworkConnection oldOwner = netIdentity.clientAuthorityOwner;

        if (oldOwner == player.connectionToClient) return;
        else
        {
            if (oldOwner != null) netIdentity.RemoveClientAuthority(oldOwner);
            netIdentity.AssignClientAuthority(player.connectionToClient);
        }
        Debug.Log("[Fin] CmdChangeAuthority");
    }

    // Network methods for EditObjet.cs

    [Command]
    public void CmdChangeColor(Color color, NetworkInstanceId objNetId)
    {
        GameObject localObj = NetworkServer.FindLocalObject(objNetId);
        localObj.GetComponent<Renderer>().material.color = color;

        RpcChangeColor(color, objNetId);
    }

    [ClientRpc]
    private void RpcChangeColor(Color color, NetworkInstanceId objNetId)
    {
        GameObject localObj = ClientScene.FindLocalObject(objNetId);
        localObj.GetComponent<Renderer>().material.color = color;
    }

    [Command]
    public void CmdChangeSize(Vector3 size, NetworkInstanceId objNetId)
    {
        GameObject localObj = NetworkServer.FindLocalObject(objNetId);
        localObj.transform.localScale = size;

        RpcChangeSize(size, objNetId);
    }

    [ClientRpc]
    private void RpcChangeSize(Vector3 size, NetworkInstanceId objNetId)
    {
        GameObject localObj = ClientScene.FindLocalObject(objNetId);
        localObj.transform.localScale = size;
    }

    [Command]
    public void CmdChangeGravity(bool g, NetworkInstanceId objNetId, string userName)
    {
        RpcChangeGravity(g, objNetId, userName);
    }

    [ClientRpc]
    private void RpcChangeGravity(bool g, NetworkInstanceId objNetId, string userName)
    {
        GameObject localObj = ClientScene.FindLocalObject(objNetId);
        localObj.GetComponent<Rigidbody>().useGravity = g;
        localObj.GetComponent<Rigidbody>().isKinematic = !g;
    }

    [Command]
    public void CmdChangeLock(bool b, NetworkInstanceId objNetId, string userName)
    {
        /*if (playerId != playerControllerId)
        {
            GameObject localObj = NetworkServer.FindLocalObject(objNetId);
            localObj.GetComponent<DragDropObject>().locked = b;
        }*/
        RpcChangeLock(b, objNetId, userName);
    }

    [ClientRpc]
    private void RpcChangeLock(bool b, NetworkInstanceId objNetId, string userName)
    {
        if (userName.Equals(UserInfo.userName, System.StringComparison.InvariantCultureIgnoreCase)) b = false;
        GameObject localObj = ClientScene.FindLocalObject(objNetId);
        localObj.GetComponent<DragDropObject>().locked = b;
    }

    [Command]
    public void CmdDeleteObject(GameObject spawnedObject)
    {
        if (spawnedObject != null) NetworkServer.Destroy(spawnedObject);
    }

    [Command]
    public void CmdUpdateObjectName(string name, NetworkInstanceId objNetId)
    {
        GameObject localObj = ClientScene.FindLocalObject(objNetId);
        localObj.GetComponent<DragDropObject>().objectName = name;
    }

}
