﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ConnectionController : MonoBehaviour {

    [SerializeField] private NetworkManager net;

    void Start ()
    {
        net.networkPort = NetInfo.port;
        net.networkAddress = NetInfo.ip;

		switch (NetInfo.state)
        {
            case NetInfo.State.CLIENT:
                net.networkPort = NetInfo.port;
                net.networkAddress = NetInfo.ip;
                net.StartClient();
                break;

            case NetInfo.State.HOST:
                net.networkPort = NetInfo.port;
                net.networkAddress = "localhost";
                net.StartHost();
                Invoke("AutomaticLogInAsAdministrator", 0.5f);
                break;

            case NetInfo.State.SERVER:
                net.networkPort = NetInfo.port;
                net.networkAddress = "localhost";
                net.StartServer();
                Invoke("AutomaticLogInAsAdministrator", 0.5f);
                break;

            default:
                Debug.LogError("ConnectionController -> Debug -> Error message -> Not role specified");
                break;
        }
	}

    private void AutomaticLogInAsAdministrator()
    {
        NetworkManager.singleton.client.connection.playerControllers[0].gameObject.GetComponent<LoginService>().AdminLogIn();
    }

}
