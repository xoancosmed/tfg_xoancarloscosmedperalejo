﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class OnlineListController : NetworkBehaviour
{
    private Text onlinePlayersText;

    void Start()
    {

        if (!isServer) return;

        NetworkServer.RegisterHandler(MsgType.Connect, OnConnected);
        NetworkServer.RegisterHandler(MsgType.Disconnect, OnDisconnected);

        InvokeRepeating("UpdateList", 7.5f, 15f);

    }

    private void OnConnected(NetworkMessage netMsg)
    {
        Debug.Log("OnConnected");
    }

    private void OnDisconnected(NetworkMessage netMsg)
    {
        Debug.Log("OnDisconnected");

        try
        {
            Destroy(netMsg.conn.playerControllers[0].gameObject);
            NetworkServer.Destroy(netMsg.conn.playerControllers[0].gameObject);
        }
        catch
        {

        }
    }

    [ClientRpc]
    public void RpcSynchronizeOnlineUsers(string[] onlineList)
    {
        if (onlinePlayersText == null) onlinePlayersText = GameObject.Find("OnlineList Text").GetComponent<Text>();
        onlinePlayersText.text = "";
        foreach (string userName in onlineList)
        {
            onlinePlayersText.text += userName;
            onlinePlayersText.text += "\n";
        }
    }

    private void UpdateList()
    {
        List<string> playersList = new List<string>();
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            playersList.Add(player.GetComponent<ChatUser>().userName);
        }

        if (onlinePlayersText == null) onlinePlayersText = GameObject.Find("OnlineList Text").GetComponent<Text>();
        onlinePlayersText.text = "";
        foreach (string userName in playersList)
        {
            onlinePlayersText.text += userName;
            onlinePlayersText.text += "\n";
        }

        RpcSynchronizeOnlineUsers(playersList.ToArray());
    }
}