﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstantiateMenu : MonoBehaviour {

    [SerializeField] private GameObject instanciatePanel;
    [SerializeField] private GameObject instanciateButton;

    private PlayerController playerController;

    void Start()
    {
        instanciatePanel.SetActive(false);
        instanciateButton.GetComponent<Image>().color = Color.white;
    }

    public void ShowHidePanel()
    {
        if (instanciatePanel.activeSelf){
            instanciatePanel.SetActive(false);
            instanciateButton.GetComponent<Image>().color = Color.white;
        } 
        else
        {
            instanciatePanel.SetActive(true);
            instanciateButton.GetComponent<Image>().color = Color.gray;
        } 
    }

    public void RequestInstanciateObject(int num)
    {
        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdNetworkInstanciate(num);

        instanciatePanel.SetActive(false);
        instanciateButton.GetComponent<Image>().color = Color.white;
    }

}
