﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class LoginServer : MonoBehaviour {

    // VARIABLES

    private List<UserData> users;

    // UNITY METHODS

	void Start () {

        Load();

    }

    // PUBLIC METHODS

    public bool CheckAccount (string userName, string password) {

        userName = userName.Trim().ToLower();

        foreach(UserData user in users)
            if (user.userName.Equals(userName))
                if (user.password.Equals(Hash_SHA256(password)))
                    return true;

        return false;

    }

    public bool NewAccount (string userName, string password) {

        if (userName.Equals("admin", System.StringComparison.InvariantCultureIgnoreCase)) return false;

        userName = userName.Trim().ToLower();

        foreach (UserData user in users)
            if (user.userName.Equals(userName))
                return false;

        UserData newUser = new UserData();
        newUser.userName = userName;
        newUser.password = Hash_SHA256(password);
        users.Add(newUser);

        Save();

        return true;

    }

    // PRIVATE METHODS

    private void Save() {

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream fileStream = File.Open(Application.persistentDataPath + "/LoginServer.dat", FileMode.OpenOrCreate);

        LoginData data = new LoginData();
        data.users = users.ToArray();

        binaryFormatter.Serialize(fileStream, data);
        fileStream.Close();

        Debug.Log("Guardado en " + Application.persistentDataPath + "/LoginServer.dat");
    }

    private void Load() {

        if (File.Exists(Application.persistentDataPath + "/LoginServer.dat")) {

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Open(Application.persistentDataPath + "/LoginServer.dat", FileMode.Open);

            LoginData data = (LoginData)binaryFormatter.Deserialize(fileStream);
            users = new List<UserData>(data.users);

            fileStream.Close();

            Debug.Log("Cargado de " + Application.persistentDataPath + "/LoginServer.dat");

        } else {
            users = new List<UserData>();
            Debug.Log("No hay datos guardados!");
        }

    }

    private static string Hash_SHA256(string s)
    {
        if (s == null) return null;

        HashAlgorithm hasher = SHA256.Create();
        byte[] hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(s));

        StringBuilder hashStringB = new StringBuilder();
        foreach (byte b in hash) hashStringB.Append(b.ToString("X2"));

        return hashStringB.ToString();
    }

    // STRUCTURES AND CLASSES

    [System.Serializable] private struct UserData {

        public string userName;
        public string password;

    }

    [System.Serializable] private class LoginData {

        public UserData[] users;

    }

}
