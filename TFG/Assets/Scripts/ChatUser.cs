﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ChatUser : NetworkBehaviour
{

    private Chat chat;

    [SyncVar]
    public string userName = "";

    void Update()
    {

        if (!isLocalPlayer) return;

        try
        {
            if (chat == null) chat = GameObject.Find("Chat").GetComponent<Chat>();
            if (userName == "")
            {
                userName = UserInfo.userName;
                CmdUpdateUserName(UserInfo.userName);
            }
        }
        catch
        {
            return;
        }

        //if (Input.GetKeyDown(KeyCode.RightShift)) chat.EscribirMensajeFalso("Texto " + cont++);

        // When the user press "Enter" while the chat is showing, we send the message
        if (Input.GetKeyDown(KeyCode.Return) && chat.showingChat && chat.fieldChat.text != "")
        {
            chat.SaveMessage(chat.fieldChat.text, UserInfo.userName);
            CmdSendMessage(chat.fieldChat.text, UserInfo.userName);
            chat.fieldChat.text = "";
        }

    }

    [Command]
    private void CmdUpdateUserName(string newUserName)
    {
        userName = newUserName;
    }

    [Command]
    public void CmdSendMessage(string message, string author)
    {
        //Debug.Log("START Cmd - Server: " + isServer + "; Client: " + isClient + "; NetID: " + netId + "; Message: " + message + "; Author: " + author + "; UseName: " + UserInfo.userName);
        RpcReceiveMessage(message, author);
    }

    [ClientRpc]
    public void RpcReceiveMessage(string message, string author)
    {
        //Debug.Log("START Rpc - Server: " + isServer + "; Client: " + isClient + "; NetID: " + netId + "; Message: " + message + "; Author: " + author + "; UseName: " + UserInfo.userName);
        if (UserInfo.userName != author)
        {
            try
            {
                if (chat == null) chat = GameObject.Find("Chat").GetComponent<Chat>();
            }
            catch
            {
                return;
            }
            chat.SaveMessage(message, author);
        }
    }

}
