﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LoginService : NetworkBehaviour {

    // VARIABLES

    [SerializeField] private GameObject prefabLoginServer;

    private LoginServer loginServer = null;

    private GameObject loginPanel = null;
    private InputField userNameInput = null;
    private InputField passwordInput = null;
    private Button loginButton = null;
    private Button signupButton = null;
    private GameObject messagePanel = null;
    private Text messageText = null;
    private Button messageButton = null;

    // UNITY METHODS
	
	void Start () {

        /*Debug.Log(netId);
        Debug.Log(isServer);
        Debug.Log(isClient);
        Debug.Log(isLocalPlayer);*/

        GetComponent<PlayerController>().enabled = false;
        GetComponent<ChatUser>().enabled = false;
        //gameObject.transform.Find("Body").gameObject.SetActive(false);

        if (!isServer) return;

        loginServer = Instantiate(prefabLoginServer, new Vector3(), Quaternion.identity).GetComponent<LoginServer>();

	}

    void Update()
    {
        if (!isLocalPlayer) return;

        if (loginPanel != null)
            if (!loginPanel.activeSelf)
                return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (NetInfo.state == NetInfo.State.CLIENT) NetworkManager.singleton.StopClient();
            else if (NetInfo.state == NetInfo.State.HOST) NetworkManager.singleton.StopHost();
            else if (NetInfo.state == NetInfo.State.SERVER) NetworkManager.singleton.StopServer();

            #if UNITY_EDITOR
             UnityEditor.EditorApplication.isPlaying = false;
            #else
               Application.Quit();
            #endif
        }

        try
        {
            if (loginPanel == null) loginPanel = GameObject.Find("Login");
            if (userNameInput == null) userNameInput = GameObject.Find("UserNameInput").GetComponent<InputField>();
            if (passwordInput == null) passwordInput = GameObject.Find("PasswordInput").GetComponent<InputField>();
            if (loginButton == null) {
                loginButton = GameObject.Find("LogInButton").GetComponent<Button>();
                if (loginButton != null) loginButton.onClick.AddListener(LogIn);
            }
            if (signupButton == null) {
                signupButton = GameObject.Find("SignUpButton").GetComponent<Button>();
                if (signupButton != null) signupButton.onClick.AddListener(SignUp);
            }
            if (messageText == null) messageText = GameObject.Find("MessageText").GetComponent<Text>();
            if (messageButton == null) {
                messageButton = GameObject.Find("MessageButton").GetComponent<Button>();
                if (messageButton != null) messageButton.onClick.AddListener(CloseMessage);
            }
            if ((messageText != null) && (messageButton != null) && (messagePanel == null))
            {
                messagePanel = GameObject.Find("MessagePanel");
                messagePanel.SetActive(false);
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (userNameInput.isFocused)
                {
                    passwordInput.Select();
                    passwordInput.ActivateInputField();
                }
                else
                {
                    userNameInput.Select();
                    userNameInput.ActivateInputField();
                }
            }

            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                if (messagePanel.activeSelf)
                {
                    messagePanel.SetActive(false);
                }
                else if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.AltGr))
                {
                    SignUp();
                }
                else
                {
                    LogIn();
                }
            }
        }
        catch
        {

        }
    }

    // PUBLIC METHODS

    public void LogIn ()
    {
        CmdLogIn(userNameInput.text, passwordInput.text);
    }

    public void SignUp()
    {
        CmdSignUp(userNameInput.text, passwordInput.text);
    }

    public void CloseMessage()
    {
        messagePanel.SetActive(false);
    }

    public void AdminLogIn()
    {
        UserInfo.userName = "Admin";
        //gameObject.transform.Find("Body").gameObject.SetActive(true);
        GetComponent<PlayerController>().enabled = true;
        GetComponent<ChatUser>().enabled = true;
        GetComponent<ChatUser>().userName = "";
        GameObject.Find("Login").SetActive(false);
        gameObject.tag = "Player";
        CmdUpdateTag("Player");
    }

    // UNET METHODS

    [Command] private void CmdUpdateTag (string tag)
    {
        gameObject.tag = tag;
        RpcUpdateTag(tag);
    }

    [ClientRpc] private void RpcUpdateTag (string tag)
    {
        gameObject.tag = tag;
    }

    [Command]
    private void CmdLogIn (string userName, string password)
    {
        if (loginServer == null) loginServer = Instantiate(prefabLoginServer, new Vector3(), Quaternion.identity).GetComponent<LoginServer>();
        TargetLogIn(connectionToClient, loginServer.CheckAccount(userName, password), userName);
    }

    [TargetRpc]
    private void TargetLogIn (NetworkConnection target, bool access, string userName)
    {
        //Debug.Log("LogIn " + acceso);
        if (access)
        {
            UserInfo.userName = userName;
            //gameObject.transform.Find("Body").gameObject.SetActive(true);
            GetComponent<PlayerController>().enabled = true;
            GetComponent<ChatUser>().enabled = true;
            GetComponent<ChatUser>().userName = "";
            GameObject.Find("Login").SetActive(false);
            gameObject.tag = "Player";
            CmdUpdateTag("Player");
        }
        else
        {
            messageText.text = "Contraseña incorrecta";
            messagePanel.SetActive(true);
        }
    }

    [Command]
    private void CmdSignUp(string userName, string password)
    {
        if (loginServer == null) loginServer = Instantiate(prefabLoginServer, new Vector3(), Quaternion.identity).GetComponent<LoginServer>();
        TargetSignUp(connectionToClient, loginServer.NewAccount(userName, password));
    }

    [TargetRpc]
    private void TargetSignUp(NetworkConnection target, bool access)
    {
        //Debug.Log("SigUp " + acceso);
        if (access) messageText.text = "Se ha registrado con éxito";
        else messageText.text = "Ese usuario ya existe";
        messagePanel.SetActive(true);
    }

}
