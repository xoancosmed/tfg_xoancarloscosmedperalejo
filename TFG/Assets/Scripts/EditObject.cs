﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditObject : MonoBehaviour {

    [SerializeField] private InputField scaleHeightInputField;
    [SerializeField] private InputField scaleWidthInputField;
    [SerializeField] private InputField scaleWideInputField;
    [SerializeField] private InputField positionXInputField;
    [SerializeField] private InputField positionYInputField;
    [SerializeField] private InputField positionZInputField;
    [SerializeField] private InputField rotationXInputField;
    [SerializeField] private InputField rotationYInputField;
    [SerializeField] private InputField rotationZInputField;
    [SerializeField] private Toggle lockToggle;
    [SerializeField] private Toggle gravityToggle;
    [SerializeField] private InputField nameInputField;
    [SerializeField] private GameObject editObjectMenu;
    [SerializeField] private GameObject deleteConfirmationPanel;
    [SerializeField] private GameObject moveRotateScalePanel;

    private GameObject obj = null;
    private PlayerController playerController = null;

    private Vector3 originalPosition;
    private Quaternion originalRotation;
    private Vector3 originalScale;

    void Start()
    {
        obj = null;
        editObjectMenu.SetActive(false);
        deleteConfirmationPanel.SetActive(false);
        moveRotateScalePanel.SetActive(false);

        /*GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            PlayerController controller = player.GetComponent<PlayerController>();
            if (controller != null)
            {
                if (controller.isLocalPlayer)
                {
                    playerController = controller;
                    break;
                }
            }
        }*/
    }

    public void Show(GameObject newObj)
    {
        if (obj != null) obj.GetComponent<Renderer>().material.shader = Shader.Find("Standard");

        obj = newObj;
        obj.GetComponent<Renderer>().material.shader = Shader.Find("Outlined/Silhouetted Diffuse");
        obj.GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.gray);
        obj.GetComponent<Renderer>().material.SetFloat("_Outline", 0.05f);
        //obj.GetComponent<Renderer>().material.shader = Shader.Find("Custom/Outline");
        //obj.GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.gray);
        //obj.GetComponent<Renderer>().material.SetFloat("_Outline", 0.03f);

        /*width.onEndEdit.RemoveAllListeners();
        height.onEndEdit.RemoveAllListeners();
        wide.onEndEdit.RemoveAllListeners();*/
        scaleWidthInputField.onValueChanged.RemoveAllListeners();
        scaleHeightInputField.onValueChanged.RemoveAllListeners();
        scaleWideInputField.onValueChanged.RemoveAllListeners();
        scaleWidthInputField.text = obj.transform.localScale.x.ToString("#.00");
        scaleHeightInputField.text = obj.transform.localScale.y.ToString("#.00");
        scaleWideInputField.text = obj.transform.localScale.z.ToString("#.00");
        /*width.onEndEdit.AddListener(ChangeSize);
        height.onEndEdit.AddListener(ChangeSize);
        wide.onEndEdit.AddListener(ChangeSize);*/
        scaleWidthInputField.onValueChanged.AddListener(ChangeSize);
        scaleHeightInputField.onValueChanged.AddListener(ChangeSize);
        scaleWideInputField.onValueChanged.AddListener(ChangeSize);

        /*width.onEndEdit.RemoveAllListeners();
        height.onEndEdit.RemoveAllListeners();
        wide.onEndEdit.RemoveAllListeners();*/
        positionXInputField.onValueChanged.RemoveAllListeners();
        positionYInputField.onValueChanged.RemoveAllListeners();
        positionZInputField.onValueChanged.RemoveAllListeners();
        positionXInputField.text = obj.transform.localPosition.x.ToString("#.00");
        positionYInputField.text = obj.transform.localPosition.y.ToString("#.00");
        positionZInputField.text = obj.transform.localPosition.z.ToString("#.00");
        /*width.onEndEdit.AddListener(ChangeSize);
        height.onEndEdit.AddListener(ChangeSize);
        wide.onEndEdit.AddListener(ChangeSize);*/
        positionXInputField.onValueChanged.AddListener(ChangePosition);
        positionYInputField.onValueChanged.AddListener(ChangePosition);
        positionZInputField.onValueChanged.AddListener(ChangePosition);

        /*width.onEndEdit.RemoveAllListeners();
        height.onEndEdit.RemoveAllListeners();
        wide.onEndEdit.RemoveAllListeners();*/
        rotationXInputField.onValueChanged.RemoveAllListeners();
        rotationYInputField.onValueChanged.RemoveAllListeners();
        rotationZInputField.onValueChanged.RemoveAllListeners();
        rotationXInputField.text = obj.transform.eulerAngles.x.ToString("#.00");
        rotationYInputField.text = obj.transform.eulerAngles.y.ToString("#.00");
        rotationZInputField.text = obj.transform.eulerAngles.z.ToString("#.00");
        /*width.onEndEdit.AddListener(ChangeSize);
        height.onEndEdit.AddListener(ChangeSize);
        wide.onEndEdit.AddListener(ChangeSize);*/
        rotationXInputField.onValueChanged.AddListener(ChangeRotation);
        rotationYInputField.onValueChanged.AddListener(ChangeRotation);
        rotationZInputField.onValueChanged.AddListener(ChangeRotation);

        gravityToggle.onValueChanged.RemoveAllListeners();
        gravityToggle.onValueChanged.AddListener(ChangeGravity);
        gravityToggle.isOn = obj.GetComponent<Rigidbody>().useGravity;
        lockToggle.onValueChanged.RemoveAllListeners();
        lockToggle.onValueChanged.AddListener(ChangeLock);

        nameInputField.onEndEdit.RemoveAllListeners();
        nameInputField.text = obj.GetComponent<DragDropObject>().objectName;
        nameInputField.onEndEdit.AddListener(ChangeName);

        editObjectMenu.SetActive(true);
    }

    public void ChangeColor(Color color)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        //obj.GetComponent<Renderer>().material.color = color;

        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdChangeColor(color, obj.GetComponent<DragDropObject>().netId);
    }

    public void ChangeColorStr(string color)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        if (color.Equals("red")) ChangeColor(Color.red);
        else if (color.Equals("blue")) ChangeColor(Color.blue);
        else if (color.Equals("green")) ChangeColor(Color.green);
        else if (color.Equals("yellow")) ChangeColor(Color.yellow);
        else if (color.Equals("black")) ChangeColor(Color.black);
        else if (color.Equals("white")) ChangeColor(Color.white);
        else ChangeColor(Color.white);
    }

    public void ChangeSize(string s)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        Vector3 size = new Vector3(float.Parse(scaleWidthInputField.text, System.Globalization.CultureInfo.InvariantCulture),
                                                    float.Parse(scaleHeightInputField.text, System.Globalization.CultureInfo.InvariantCulture),
                                                    float.Parse(scaleWideInputField.text, System.Globalization.CultureInfo.InvariantCulture));

        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdChangeSize(size, obj.GetComponent<DragDropObject>().netId);
    }

    public void ChangePosition(string s)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        obj.transform.localPosition = new Vector3(float.Parse(positionXInputField.text, System.Globalization.CultureInfo.InvariantCulture),
                                                    float.Parse(positionYInputField.text, System.Globalization.CultureInfo.InvariantCulture),
                                                    float.Parse(positionZInputField.text, System.Globalization.CultureInfo.InvariantCulture));
    }

    public void ChangeRotation(string s)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        obj.transform.eulerAngles = new Vector3(float.Parse(rotationXInputField.text, System.Globalization.CultureInfo.InvariantCulture),
                                                    float.Parse(rotationYInputField.text, System.Globalization.CultureInfo.InvariantCulture),
                                                    float.Parse(rotationZInputField.text, System.Globalization.CultureInfo.InvariantCulture));
    }

    public void ChangeGravity(bool b)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdChangeGravity(b, obj.GetComponent<DragDropObject>().netId, UserInfo.userName); // We only change the attribute on the other clients
    }

    public void ChangeLock (bool b)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdChangeLock(b, obj.GetComponent<DragDropObject>().netId, UserInfo.userName); // We only change the attribute on the other clients
    }

    public void AskDeleteObject ()
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        deleteConfirmationPanel.SetActive(true);
    }

    public void ConfirmDeleteObject()
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        deleteConfirmationPanel.SetActive(false);
        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdDeleteObject(obj);

        obj = null;
        editObjectMenu.SetActive(false);
    }

    public void CancelDeleteObject()
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        deleteConfirmationPanel.SetActive(false);
    }

    public void ChangeName(string name)
    {
        if (obj == null)
        {
            editObjectMenu.SetActive(false);
            return;
        }
        if (playerController == null) playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.CmdUpdateObjectName(name, obj.GetComponent<DragDropObject>().netId);
    }

    public void ActivateMovement ()
    {
        originalPosition = obj.transform.position;
        originalRotation = obj.transform.rotation;
        originalScale = obj.transform.localScale;

        editObjectMenu.SetActive(false);
        moveRotateScalePanel.SetActive(true);
    }

    public void UndoMovement()
    {
        obj.transform.position = originalPosition;
        obj.transform.rotation = originalRotation;
        obj.transform.localScale = originalScale;

        // Reload the numeric scale value in the GUI
        scaleWidthInputField.onValueChanged.RemoveAllListeners();
        scaleHeightInputField.onValueChanged.RemoveAllListeners();
        scaleWideInputField.onValueChanged.RemoveAllListeners();
        scaleWidthInputField.text = obj.transform.localScale.x.ToString("#.00");
        scaleHeightInputField.text = obj.transform.localScale.y.ToString("#.00");
        scaleWideInputField.text = obj.transform.localScale.z.ToString("#.00");
        scaleWidthInputField.onValueChanged.AddListener(ChangeSize);
        scaleHeightInputField.onValueChanged.AddListener(ChangeSize);
        scaleWideInputField.onValueChanged.AddListener(ChangeSize);
    }

    public void DisableMovement ()
    {
        moveRotateScalePanel.SetActive(false);
        editObjectMenu.SetActive(true);
    }

    public void Close()
    {
        if (obj != null) obj.GetComponent<Renderer>().material.shader = Shader.Find("Standard");
        obj = null;
        editObjectMenu.SetActive(false);
    }

    public bool IsWriting()
    {
        return scaleHeightInputField.isFocused ||
               scaleWidthInputField.isFocused ||
               scaleWideInputField.isFocused ||
               nameInputField.isFocused;
    }

    public bool IsMoving()
    {
        return moveRotateScalePanel.activeSelf;
    }
    
    public bool IsEditing()
    {
        return editObjectMenu.activeSelf || moveRotateScalePanel.activeSelf;
    }

    public bool IsSelected(GameObject obj2)
    {
        return obj == obj2;
    }

}
