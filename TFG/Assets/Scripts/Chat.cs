﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Chat : NetworkBehaviour
{

    /* ********************* *
     * ***** VARIABLES ***** *
     * ********************* */

    [SerializeField] private GameObject panelChat;
    [SerializeField] private Text textChat;
    [SerializeField] public InputField fieldChat;
    [SerializeField] private GameObject showButton;
    [SerializeField] private Text showText;

    public bool showingChat = true;
    private int lastPrintedMessage = -1;
    private List<ChatMessage> chatMessages;

    private IEnumerator checkCorutine;

    private bool scrolling = false;

    /* ************************ *
     * ***** MONO METHODS ***** *
     * ************************ */

    void Start()
    {

        if (panelChat == null) panelChat = GameObject.Find("Panel Chat");
        if (textChat == null) textChat = GameObject.Find("Text Chat").GetComponent<Text>();
        if (fieldChat == null) fieldChat = GameObject.Find("InputField Chat").GetComponent<InputField>();
        if (showButton == null) showButton = GameObject.Find("Button Mostrar/Ocultar");
        if (showText == null) showText = GameObject.Find("Text Mostrar/Ocultar").GetComponent<Text>();

        checkCorutine = CheckChat(.5f);

        chatMessages = new List<ChatMessage>();
        chatMessages.Add(new ChatMessage("Bienvenido al chat!", "Admin"));

        HideShowPanel();

    }

    void Update()
    {

    }

    /* ************************** *
     * ***** PUBLIC METHODS ***** *
     * ************************** */

    public bool IsWriting()
    {
        return fieldChat.isFocused;
    }

    public bool IsScrolling()
    {
        return scrolling;
    }

    public void StartScrolling()
    {
        scrolling = true;
    }

    public void StopScrolling()
    {
        scrolling = false;
    }

    public void ShowButtonClick()
    {
        HideShowPanel();
    }

    public void SaveMessage(string message, string author)
    {
        chatMessages.Add(new ChatMessage(message, author));
    }

    /* *************************** *
     * ***** PRIVATE METHODS ***** *
     * *************************** */

    private void HideShowPanel()
    {
        showingChat = !showingChat;

        panelChat.SetActive(showingChat);
        showText.text = showingChat ? "Ocultar Chat" : "Mostrar Chat";

        if (showingChat) StartCoroutine(checkCorutine);
        else StopCoroutine(checkCorutine);
    }

    /* ********************** *
     * ***** COROUTINES ***** *
     * ********************** */


    // When the chat panel is active, this coroutine executes each "waitTime" seconds an receive and shows the new incoming messages
    IEnumerator CheckChat(float waitTime)
    {
        while (true)
        {
            while (true)
            {
                if (lastPrintedMessage >= (chatMessages.Count - 1)) break;

                lastPrintedMessage++;

                string color = "black";
                if (chatMessages[lastPrintedMessage].author == UserInfo.userName) color = "green";
                else if (chatMessages[lastPrintedMessage].author == "Admin") color = "red";

                textChat.text += "\n";
                textChat.text += "<color=" + color + ">";
                textChat.text += "<b>" + chatMessages[lastPrintedMessage].author.Replace("<", "").Replace(">", "") + ": </b>";
                textChat.text += "<i>" + chatMessages[lastPrintedMessage].message.Replace("<", "").Replace(">", "") + "</i>";
                textChat.text += "</color>";
            }
            yield return new WaitForSeconds(waitTime);
        }
    }

    /* ******************* *
     * ***** CLASSES ***** *
     * ******************* */

    private class ChatMessage
    {
        public string message;
        public string author;

        public ChatMessage(string message, string author)
        {
            this.message = message;
            this.author = author;
        }
    }
}
