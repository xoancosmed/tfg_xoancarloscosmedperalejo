﻿# TFG Entorno virtual colaborativo

## Información general
* Alumno: Xoán Carlos Cosmed Peralejo
* Título: Grado en ingeniería de las tecnologías de telecomunicación
* Centro: Universidad de Vigo

## Descripción
* Título: Interacción de usuarios remotos en un entorno virtual colaborativo creado con el motor Unity
* Descripción: Unity es un motor multiplataforma de desarrollo de videojuegos y aplicaciones de Realidad Aumentada y Realidad Virtual. La  idea  es  desarrollar  un  conjunto  de  utilidades  que  permita  la  interacción  de  usuarios  remotos  en  un entorno colaborativo de trabajo, considerando aspectos de gestión del sistema y compartición de recursos. 